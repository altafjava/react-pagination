import React, { Component } from 'react';
import './App.css';
import styles from './App.module.css';
import Axios from 'axios'


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      currentPage: 1,
      perPage: 0,
      totalPages: 0,
      total: 0
    }
  }
  componentDidMount() {
    this.apiCall(1)
  }
  renderPaginationButton = () => {
    const numbers = []
    if (this.state.currentPage !== 1)
      numbers.push(<button key='Previous' onClick={this.onPaginationButtonClick} value="Previous">Previous</button>)
    for (let i = 1; i <= this.state.totalPages; i++) {
      if (i === this.state.currentPage)
        numbers.push(<button key={i} className={styles.active} onClick={this.onPaginationButtonClick} value={i}>{i}</button>)
      else
        numbers.push(<button key={i} onClick={this.onPaginationButtonClick} value={i}>{i}</button>)
    }
    if (this.state.currentPage !== this.state.totalPages)
      numbers.push(<button key='Next' onClick={this.onPaginationButtonClick} value="Next">Next</button>)
    return numbers
  }
  onPaginationButtonClick = (e) => {
    const pageNo = e.target.value;
    if (pageNo === 'Previous') {
      this.apiCall(this.state.currentPage - 1)
    } else if (pageNo === 'Next') {
      this.apiCall(this.state.currentPage + 1)
    } else {
      this.apiCall(pageNo)
    }
  }

  render() {
    return (
      < div className={styles.app} >
        <table className={styles.table}>
          <thead>
            <tr>
              <th>S/N</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.users.map(user => {
                return (
                  <tr key={user.id}>
                    <td>{user.id}</td>
                    <td>{user.first_name}</td>
                    <td>{user.last_name}</td>
                    <td>{user.email}</td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        <div className={styles.pagination}>
          {this.renderPaginationButton()}
        </div>
        {/* <div className={styles.pagination}>
          <span onClick={this.onPaginationButtonClick}>Previous</span>
          <span className={styles.active}>1</span>
          <button onClick={this.onPaginationButtonClick} value="2">2</button>
          <span onClick={this.onPaginationButtonClick} value="3">3</span>
          <span>4</span>
          <span>Next</span>
        </div> */}
      </div >
    );
  }
  apiCall = (pageNo) => {
    const url = "https://reqres.in/api/users?page=" + pageNo;
    Axios.get(url)
      .then(response => {
        const data = response.data
        this.setState({
          users: data.data,
          currentPage: data.page,
          perPage: data.per_page,
          totalPages: data.total_pages,
          total: data.total
        })
      })
  }
}
export default App;
